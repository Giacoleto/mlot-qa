﻿using OpenQA.Selenium;
using System;
using WorkshopQA.Pages;

namespace WorkshopQA.Utils
{
    class Clickers : BaseClass
    {
        public Clickers(IWebDriver _driver) : base(_driver) { }

        public void Click(string clickOn)
        {
            MlotPage mlotPage = new MlotPage(_driver);
            PostPage postPage = new PostPage(_driver);
            
            switch (clickOn)
            {
                case "Log Out":
                    mlotPage.logOutLink.Click();
                    break;
                case "Log In":
                    mlotPage.logInLink.Click();
                    break;
                case "Add Post":
                    mlotPage.addPostLink.Click();
                    break;
                case "Delete Post":
                    postPage.deletePostButton.Click();
                    break;
                default:
                    Console.WriteLine("No Such Click");
                    break;
            }
        }
    }
}
