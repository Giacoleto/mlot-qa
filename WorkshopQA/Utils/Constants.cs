﻿using System;

namespace WorkshopQA
{
    public static class Constants
    {
        // Public Values
        public static string userName => "WorkshopQA";
        public static string userNameSecond => "WorkshopQA02";
        public static string userPass => "Parola01";
        public static int uuid => Convert.ToInt32(DateTime.Now.TimeOfDay.TotalSeconds);

        // Public Title and Text Values
        public static string postTitle => "Post Title";
        public static string postText => "Post Text";
        public static string postTitleEdit => "Edited Post Text";
        public static string postTextEdit => "Edited Post Title";
        public static string postCustomTitle => "Custom Post Title" + uuid;
        public static string postCustomText => "Custom Post Text" + uuid;  
    }
}
