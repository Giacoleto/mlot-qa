﻿using OpenQA.Selenium;
using WorkshopQA.Pages;

namespace WorkshopQA.Utils
{
    public class Helpers : BaseClass
    {
        public Helpers(IWebDriver _driver) : base(_driver) { }

        public void NavigateToUrl(string url)
        {
            _driver.Navigate().GoToUrl(url);
        }
    }
}
