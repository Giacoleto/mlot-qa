﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkshopQA.Pages;
using WorkshopQA.Utils;

namespace WorkshopQA.Tests
{
    class ArticleTests : BaseTest
    {
        [TestClass]
        public class MainTest : BaseTest
        {
            // Create a New Post ( Happy Path )
            [TestMethod]       
            public void PostTestSuccess()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);
                PostPage postPage = new PostPage(_driver);
                Clickers clickers = new Clickers(_driver);

                clickers.Click("Log In");
                loginPage.Login(Constants.userName, Constants.userPass);
                clickers.Click("Add Post");
                postPage.AddPost(Constants.postTitle, Constants.postText);

                Assert.IsTrue(postPage.currentPostTitle.Text.Equals(Constants.postTitle));
                Assert.IsTrue(postPage.currentPostText.Text.Equals(Constants.postText));               
            }

            // Edit a Post
            [TestMethod]
            public void PostEditSuccess()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);
                PostPage postPage = new PostPage(_driver);
                Clickers clickers = new Clickers(_driver);

                clickers.Click("Log In");
                loginPage.Login(Constants.userName, Constants.userPass);
                clickers.Click("Add Post");
                postPage.AddPost(Constants.postTitle, Constants.postText);
                postPage.EditPost(Constants.postTitleEdit, Constants.postTextEdit);

                Assert.IsTrue(postPage.currentPostTitle.Text.Equals(Constants.postTitleEdit));
                Assert.IsTrue(postPage.currentPostText.Text.Equals(Constants.postTextEdit));
            }

            // Delete a Post
            [TestMethod]
            public void PostDeleteSuccess()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);
                PostPage postPage = new PostPage(_driver);
                Clickers clickers = new Clickers(_driver);

                clickers.Click("Log In");
                loginPage.Login(Constants.userName, Constants.userPass);
                clickers.Click("Add Post");
                postPage.AddPost(Constants.postCustomText, Constants.postCustomTitle);
                clickers.Click("Delete Post");

                Assert.IsFalse(postPage.currentPostTitle.Text.Equals(Constants.postCustomText));                
            }

            // Create a post with missing Title or Text
            [TestMethod]
            public void PostMisssingTitleAndTextAddFail()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);
                PostPage postPage = new PostPage(_driver);
                Clickers clickers = new Clickers(_driver);

                clickers.Click("Log In");
                loginPage.Login(Constants.userName, Constants.userPass);
                clickers.Click("Add Post");
                postPage.AddPost(" ", " ");

                string missingTitle = postPage.emptyPostTitleWarningMessages.Text;
                string missingText = postPage.emptyPostTextWarningMessages.Text;

                Assert.IsTrue(missingTitle.Contains("This field is required."));
                Assert.IsTrue(missingText.Contains("This field is required."));
            }

            // Anon users should be unnable to edit a Post
            [TestMethod]
            public void PostEditAnonUserShouldBeRedirected()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);
                PostPage postPage = new PostPage(_driver);
                Helpers helpers = new Helpers(_driver);
                Clickers clickers = new Clickers(_driver);

                clickers.Click("Log In");
                loginPage.Login(Constants.userName, Constants.userPass);
                clickers.Click("Add Post");
                postPage.AddPost(Constants.postCustomTitle, Constants.postCustomText);
                string postNumber = postPage.postNumberGetByUrl();
                clickers.Click("Log Out");
                string lastPostEditUrl = BaseClass.baseUrl + "/edit/" + postNumber;
                helpers.NavigateToUrl(lastPostEditUrl);
                string currentUrl = _driver.Url;
                
                Assert.AreEqual(currentUrl, lastPostEditUrl);
            }

            // There should be a Post length limit
            [TestMethod]
            public void PostPreviewTextSizeLimit()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);
                PostPage postPage = new PostPage(_driver);
                Helpers helpers = new Helpers(_driver);
                Clickers clickers = new Clickers(_driver);

                clickers.Click("Log In");
                loginPage.Login(Constants.userName, Constants.userPass);
                clickers.Click("Add Post");
                postPage.LongPostText(Constants.postTitle, Constants.postCustomText);
                string postNumber = postPage.postNumberGetByUrl();

                string newPostText = postPage.currentPostText.Text;
                int newPostTextSize = newPostText.Length;
                int newPostMaxSize = 600;

                string lastPostUrl = BaseClass.baseUrl + "/post/" + postNumber;
                helpers.NavigateToUrl(lastPostUrl);
                clickers.Click("Delete Post");

                Assert.IsTrue(newPostTextSize < newPostMaxSize, "The Text length is larger than the accepted value");
            }

            // Anon user should be unnable to delete a Post
            [TestMethod]
            public void PostCanBeDeletedByAnonUser()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);
                PostPage postPage = new PostPage(_driver);
                Helpers helpers = new Helpers(_driver);
                Clickers clickers = new Clickers(_driver);

                clickers.Click("Log In");
                loginPage.Login(Constants.userName, Constants.userPass);
                clickers.Click("Add Post");
                postPage.AddPost(Constants.postCustomTitle, Constants.postCustomText);
                string postNumber = postPage.postNumberGetByUrl();
                clickers.Click("Log Out");
                string lastPostDeleteUrl = BaseClass.baseUrl + "/post/" + postNumber + "/delete";
                helpers.NavigateToUrl(lastPostDeleteUrl);

                string lastPostUrl = BaseClass.baseUrl + "/post/" + postNumber;
                helpers.NavigateToUrl(lastPostUrl);

                Assert.IsTrue(postPage.currentPostTitle.Text.Equals(Constants.postCustomTitle));                
            }

            // Editing a Post will change the original author
            [TestMethod]
            public void PostAuthorIsNotChanged()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);
                PostPage postPage = new PostPage(_driver);
                Helpers helpers = new Helpers(_driver);
                Clickers clickers = new Clickers(_driver);

                // Login and add a post
                clickers.Click("Log In");
                loginPage.Login(Constants.userName, Constants.userPass);
                clickers.Click("Add Post");
                postPage.AddPost(Constants.postCustomTitle, Constants.postCustomText);

                // Get post number, author and logout
                
                string postNumber = postPage.postNumberGetByUrl();
                helpers.NavigateToUrl(baseUrl);
                string postOriginalAuthor = Constants.userName;
                clickers.Click("Log Out");

                // Login with different user and edit the previous post
                clickers.Click("Log In"); 
                loginPage.Login(Constants.userNameSecond, Constants.userPass);
                string lastPostUrl = BaseClass.baseUrl + "/post/" + postNumber;
                helpers.NavigateToUrl(lastPostUrl);
                postPage.EditPost(Constants.postTitleEdit, Constants.postTextEdit);

                // Get the current Author after edit
                helpers.NavigateToUrl(baseUrl);
                string postNewAuthor = mlotPage.postDivContainers.Text;

                string postCurrentAuthor = mlotPage.postGetAuthorByIndexNumber(postNumber);

                Assert.AreEqual(postOriginalAuthor, postCurrentAuthor);        
            }
        }
    }
}
