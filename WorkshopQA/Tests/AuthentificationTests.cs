﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkshopQA.Pages;

namespace WorkshopQA.Tests
{
    class AuthentificationTests : BaseTest
    {
        [TestClass]
        public class MainTest: BaseTest
        {
            [TestMethod]
            public void SignUpTestSuccess()
            {                           
                SignUpPage signUpPage = new SignUpPage(_driver);
                MlotPage mlotPage = new MlotPage(_driver);

                string userName = Constants.userName + Constants.uuid;
                string userPassConfirmation = Constants.userName;
                signUpPage.ValidSignUp(userName, Constants.userPass, userPassConfirmation);

                Assert.AreEqual(mlotPage.loggedInUserName.Text, "Logged in as: " + userName);
            }

            [TestMethod]
            public void SignUpTestExistingUsername()
            {
                SignUpPage signUpPage = new SignUpPage(_driver);

                string userPassConfirmation = Constants.userName;
                signUpPage.ValidSignUp(Constants.userName, Constants.userPass, userPassConfirmation);
                string actualvalue = signUpPage.usernameExistsWarningMessage.Text;

                Assert.IsTrue(actualvalue.Contains("A user with that username already exists"));
            }

            [TestMethod]
            public void SignUpTestConfirmPasswordFail()
            {
                SignUpPage signUpPage = new SignUpPage(_driver);

                string userPassConfirmation = Constants.userName;
                signUpPage.ValidSignUp(Constants.userName + Constants.uuid, Constants.userPass, userPassConfirmation + Constants.uuid);                
                string actualvalue = signUpPage.passwordMissmatchWarningMessage.Text;

                Assert.IsTrue(actualvalue.Contains("The two password fields didn't match.")); 
            }

            [TestMethod]
            public void SignUpTestAllNumericPassword()
            {
                SignUpPage signUpPage = new SignUpPage(_driver);
              
                signUpPage.ValidSignUp(Constants.userName, "9999" + Constants.uuid, "9999" + Constants.uuid);
                string actualvalue = signUpPage.numericPasswordWarningMessage.Text;

                Assert.IsTrue(actualvalue.Contains("This password is entirely numeric"));
            }

            [TestMethod]
            public void SignUpTestIdenticalUsernameAndPassword()
            {
                SignUpPage signUpPage = new SignUpPage(_driver);

                signUpPage.ValidSignUp(Constants.userName, Constants.userName, Constants.userName);
                string actualvalue = signUpPage.similarUsernamePasswordWarningMessage.Text;

                Assert.IsTrue(actualvalue.Contains("The password is too similar to the username."));
            }

            [TestMethod]
            public void SignUpTestSimilarUsernameAndPassword()
            {
                SignUpPage signUpPage = new SignUpPage(_driver);

                signUpPage.ValidSignUp(Constants.userName + Constants.uuid, Constants.uuid + Constants.userName, Constants.uuid + Constants.userName);
                string actualvalue = signUpPage.similarUsernamePasswordWarningMessage.Text;

                Assert.IsTrue(actualvalue.Contains("The password is too similar to the username"));
            }

            [TestMethod]
            public void LoginTestSuccessful()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);

                loginPage.Login(Constants.userName, Constants.userPass);

                Assert.IsTrue(mlotPage.loggedInUserName.Text.Equals("Logged in as: " + Constants.userName));
            }

            [TestMethod]
            public void LoginTestWrongUsername()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);

                loginPage.Login(Constants.userName + "999", Constants.userPass);
               
                string actualvalue = loginPage.loginValidationWarningMessage.Text;
                Assert.IsTrue(actualvalue.Contains("Your username and password didn't match."));
                Assert.IsFalse(mlotPage.loggedInUserName.Text.Equals("Logged in as: " + Constants.userName));                
            }

            [TestMethod]
            public void LoginTestWrongPassword()
            {
                MlotPage mlotPage = new MlotPage(_driver);
                LoginPage loginPage = new LoginPage(_driver);

                loginPage.Login(Constants.userName, Constants.userPass + "123");
             
                string actualvalue = loginPage.loginValidationWarningMessage.Text;
                Assert.IsTrue(actualvalue.Contains("Your username and password didn't match."));
                Assert.IsFalse(mlotPage.loggedInUserName.Text.Equals("Logged in as: " + Constants.userName));
            }
        }
    }
}
