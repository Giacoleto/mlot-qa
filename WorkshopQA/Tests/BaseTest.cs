﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace WorkshopQA.Tests
{
    [TestClass]
    public class BaseTest
    {
        public IWebDriver _driver;

        public string baseUrl = "http://bodeamariuscosmin.pythonanywhere.com/mlot";

        [TestInitialize]
        public void BrowserInitialization()
        {
            _driver = new ChromeDriver();

            // Go To URL
            _driver.Navigate().GoToUrl(baseUrl);
        }

        [TestCleanup]
        public void TearDown()
        {
            _driver.Close();
        }
    }
}
