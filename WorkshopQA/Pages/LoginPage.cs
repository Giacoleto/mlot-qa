﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace WorkshopQA.Pages
{
    class LoginPage : BaseClass
    {

        public string loginUrl = BaseClass.baseUrl + "/login";

        [FindsBy(How = How.Id, Using = "id_username")]
        public IWebElement loginUsername;

        [FindsBy(How = How.Id, Using = "id_password")]
        public IWebElement loginPassword;

        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > div > p")]
        public IWebElement loginValidationWarningMessage;

        [FindsBy(How = How.CssSelector, Using = "[type='submit']")]
        public IWebElement loginButton;
        public LoginPage(IWebDriver _driver) : base(_driver) { }
        public void Login(string username, string password)
        {           
            MlotPage mlotPage = new MlotPage(_driver);
            LoginPage loginPage = new LoginPage(_driver);

            mlotPage.logInLink.Click();

            loginPage.loginUsername.SendKeys(username);
            loginPage.loginPassword.SendKeys(password);
            loginPage.loginButton.Click();
        }
    }
}
