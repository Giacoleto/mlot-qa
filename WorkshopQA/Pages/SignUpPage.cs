﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace WorkshopQA.Pages
{
    class SignUpPage : BaseClass
    {
        [FindsBy(How = How.Id, Using = "id_username")]
        public IWebElement userNameField;

        [FindsBy(How = How.Id, Using = "id_password1")]
        public IWebElement passwordField;

        [FindsBy(How = How.Id, Using = "id_password2")]
        public IWebElement confirmPasswordField;

        [FindsBy(How = How.CssSelector, Using = "[type='submit']")]
        public IWebElement submitSignUpButton;

        [FindsBy(How = How.CssSelector, Using = "#content > div > ul > li:nth-child(1) > a")]
        public IWebElement loggedInUser;

        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > form > p:nth-child(3)")]
        public IWebElement usernameExistsWarningMessage;

        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > form > p:nth-child(5)")]
        public IWebElement passwordMissmatchWarningMessage, similarUsernamePasswordWarningMessage;

        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > form > p:nth-child(6)")]
        public IWebElement numericPasswordWarningMessage;
        public SignUpPage(IWebDriver _driver) : base(_driver) { }

        public void ValidSignUp(string userName, string userPass, string userPassConfirmation)
        {         
            SignUpPage signUp = new SignUpPage(_driver);
            MlotPage mlotPage = new MlotPage(_driver);

            mlotPage.signUpLink.Click();

            signUp.userNameField.SendKeys(userName);
            signUp.passwordField.SendKeys(userPass);
            signUp.confirmPasswordField.SendKeys(userPassConfirmation);
            signUp.submitSignUpButton.Click();
        }
    }
}

