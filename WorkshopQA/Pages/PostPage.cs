﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Text.RegularExpressions;

namespace WorkshopQA.Pages
{
    class PostPage : BaseClass
    {
        #region New Post Page

        // New Post page URL
        string newPostUrl = BaseClass.baseUrl + "/post/new/";

        // New Post Title TextField
        [FindsBy(How = How.Id, Using = "id_title")]
        public IWebElement postTitleField;

        // New Post Text TextField
        [FindsBy(How = How.Id, Using = "id_text")]
        public IWebElement postTextField;

        // New Post Bottom Save Button
        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > form > button")]
        public IWebElement postSaveButton;

        #endregion

        #region Current Post Page

        // Current Post Title Header
        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > div > h1")]
        public IWebElement currentPostTitle;

        // Current Post Text Header
        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > div > p")]
        public IWebElement currentPostText;

        // Current Post Edit Button
        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > div > a:nth-child(3) > span")]
        public IWebElement editPostButton;

        // Current Post Delete Button
        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > div > a:nth-child(4) > span")]
        public IWebElement deletePostButton;

        #endregion

        #region Edit Post Page

        // Need to get post <Index> nr.

        #endregion

        #region Warning Messages

        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > form > ul:nth-child(2) > li")]
        public IWebElement emptyPostTitleWarningMessages;

        [FindsBy(How = How.CssSelector, Using = "body > div.container-box > form > ul:nth-child(4) > li")]
        public IWebElement emptyPostTextWarningMessages;

        #endregion

        public PostPage(IWebDriver _driver) : base(_driver) { }

        public void AddPost(string postTitle, string postText)
        {
            PostPage postPage = new PostPage(_driver);           

            postPage.postTitleField.SendKeys(postTitle);
            postPage.postTextField.SendKeys(postText);
            postPage.postSaveButton.Click();
        }

        public void EditPost(string postEditTitle, string postEditText)
        {
            PostPage postPage = new PostPage(_driver);

            postPage.editPostButton.Click();
            postPage.postTitleField.Clear();
            postPage.postTextField.Clear();

            postPage.postTitleField.SendKeys(postEditTitle);
            postPage.postTextField.SendKeys(postEditText);
            postPage.postSaveButton.Click();
        }

        public void LongPostText(string postTitle, string longText)
        {
            PostPage postPage = new PostPage(_driver);

            postPage.postTitleField.SendKeys(postTitle);

            for (int i = 1; i < 100; i++)
            {
                postPage.postTextField.SendKeys(longText);
            }
            postPage.postSaveButton.Click();
        }

        public string postNumberGetByUrl()
        {
            string postNumber = Regex.Match(_driver.Url, @"\d+").Value;
            return postNumber; 
        }
    }
}
