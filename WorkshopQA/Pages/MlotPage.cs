﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace WorkshopQA.Pages
{
    class MlotPage : BaseClass
    {
        #region Logged In Phase
        // Current UserName Logged In 
        [FindsBy(How = How.CssSelector, Using = "#content > div > ul > li:nth-child(1) > a")]
        public IWebElement loggedInUserName;

        // Log Out User Link
        [FindsBy(How = How.CssSelector, Using = "#content > div > ul > li:nth-child(2) > a")]
        public IWebElement logOutLink;

        // Mlot ( Homepage ) Link
        [FindsBy(How = How.CssSelector, Using = "#content > div > ul > li:nth-child(4) > a")]
        public IWebElement mlotLink;

        // Add New Post Link
        [FindsBy(How = How.CssSelector, Using = "#content > div > ul > li:nth-child(5) > a > span")]
        public IWebElement addPostLink;
        #endregion

        #region Logged Out Phase
        // Sign Up Page Link
        [FindsBy(How = How.LinkText, Using = "Sign Up")]
        public IWebElement signUpLink;

        // Log In Page Link
        [FindsBy(How = How.LinkText, Using = "Log In")]
        public IWebElement logInLink;
        #endregion

        // Get Post Author
        [FindsBy(How = How.CssSelector, Using = "div > div.author")]
        public IWebElement currentAuthor;
       
        [FindsBy(How = How.CssSelector, Using = "body > div.container-box")]
        public IWebElement postDivContainers, link;

        [FindsBy(How = How.CssSelector, Using = "div > h1 > a ")]
        public IWebElement postLinkUrl;

        public MlotPage(IWebDriver _driver) : base(_driver) { }

        public string postGetAuthorByIndexNumber(string postNumber)
        {
            string postCurrentAuthor = null;

            var postList = _driver.FindElements(By.CssSelector(".container-box > .post"));

            foreach (IWebElement post in postList)
            {
                var postLink = post.FindElement(By.CssSelector("div > h1 > a "));
                string url = postLink.GetAttribute("href");

                if (url.Contains(postNumber))
                {
                    string postInformation = post.Text;
                    string authorKey = "Author:";
                    postCurrentAuthor = postInformation.Substring(postInformation.IndexOf("Author:") + authorKey.Length);
                }
            }

            return postCurrentAuthor;
        }
    }
}
