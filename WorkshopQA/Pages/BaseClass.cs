﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace WorkshopQA.Pages
{
    public class BaseClass
    {
        public static IWebDriver _driver;

        public static string baseUrl = "http://bodeamariuscosmin.pythonanywhere.com/mlot";

        public BaseClass(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(_driver, this);
        }
    }
}
